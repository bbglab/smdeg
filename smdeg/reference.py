import itertools
import logging

from bgreference import refseq


_BUILD = None


def set_build(build):
    """
    Set the build fo the reference genome. Should only be called once

    Args:
        build (str): genome reference build

    """
    global _BUILD
    _BUILD = build
    logging.getLogger(__name__).info('Using %s as reference genome', _BUILD.upper())


def get(chromosome, start, size=1):
    """
    Gets a sequence from the reference genome

    Args:
        chromosome (str): chromosome
        start (int): start position where to look
        size (int): number of bases to retrieve

    Returns:
        str. Sequence from the reference genome

    """
    return refseq(_BUILD, chromosome, start, size)


def get_kmer(chromosome, pos, size):
    """Get a kmer centered in pos"""
    return get(chromosome, pos - size // 2, size=size)



def _slicing_window(seq, n):
    """Yield items of size n through a sequence"""
    it = iter(seq)
    result = ''.join(itertools.islice(it, n))

    if len(result) == n:
        yield result

    for elem in it:
        result = result[1:] + elem
        yield result


def generate_triplets(chromosome, start, stop):
    """Yield triplets from a sequence"""
    seq = get(chromosome, start-1, stop-start+2)
    yield from _slicing_window(seq, 3)

def generate_kmers(chromosome, start, stop, kmer):
    """Yield triplets from a sequence"""
    midv = int(kmer / 2)
    seq = get(chromosome, start-midv, stop-start+midv+1)
    yield from _slicing_window(seq, kmer)
