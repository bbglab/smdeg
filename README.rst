.. _readme:

SMDeg
=====

SMDeg is a tool to identify user-defined protein regions with a significant bias of observed somatic mutations in a dataset of tumor samples. It computes the difference between the observed and expected number of non-synonymous mutations the background mutational spectra of the set of samples 
(mutational spectra of the TCGA cohorts can be found in the Downloads tab). SMDeg is a flexible tool that can be used in to address different questions. For example, in our analysis in Nature Cancer (ref) we focused on identification of degron instances with a significant enrichment of missense mutations across TCGA cohorts. In other study, we focused on the 
identification of PFAM domains with an enrichment of somatic non-synonymous mutations (ref). SMDeg requires the genomic coordinates of the regions to be queried, the genomic coordinates of the genes of interest and the dataset of somatic mutations.  


.. |sd| replace:: **smdeg**

Usage
-----

|sd| can be used through the command line interface.
Use ``-h`` or ``--help`` to access the help information.

Usage: smdeg [OPTIONS]

  Run SMDeg on the genomic regions in ELEMENTS FILE and the regions of
  interest REGIONS_FILE using the mutations in MUTATIONS FILE.

Options:
  -m, --muts MUTATIONS_FILE       Variants file  [required]
  -e, --elements ELEMENTS_FILE    Genomic elements to analyse  [required]
  -r, --regions REGIONS_FILE      Genomic regions of interest  [required]
  -s, --signature SIGNATURE_FILE  Signature file. Default equial probabilities
  -o, --output OUTPUT_FOLDER      Output folder. Default to regions file name
                                  without extensions.
  -c, --configuration CONFIG_FILE
                                  Configuration file. Default to 'smdeg.conf'
                                  in the current folder if exists or to
                                  ~/.bbglab/smdeg.conf if not.
  --debug                         Show more progress details
  --version                       Show the version and exit.
  -h, --help                      Show this message and exit.


Formats
-------

The mutation file format must be a tab-separated file (it can be compressed with gzip or not),
with a header and, at least, these fields:

- CHROMOSOME
- POSITION
- REF
- ALT
- SAMPLE


.. _readme install:

Installation
------------

.. code:: bash

   git clone https://franmj88@bitbucket.org/bbglab/smdeg.git ./destination_path/

   cd ./destination_path/

   pip install .



Run the example
---------------

# Tested in a laptop with 4 cores and 32GB of RAM

.. code:: bash

   cd ./destination_path/run_example_smdeg/

   smdeg -m LIHC.tsv.gz -o output_LIHC/ -r coordinates_degrons_formatted.tsv.gz -s LIHC.json -c smdeg.conf -e cds.regions.tsv.gz

.. _readme license:

License
-------

Apache Software License 2.0